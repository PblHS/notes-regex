/*
var exp = new RegExp('');
var exp2 = /gdjsgdjhs/;
var exp3 = /agdjag\/242342/
var exp4 = /[]/
*/
/*

\d 		cualquier caracter
\w 		cualquier letra o numero
\s 		cualquier espacio en blanco
\D 		cualquier caracter que no sea numero
\W 		cualquier caracter que no sea alfanumerico
\S 		cualquier caracter que no sea un espacio en blanco
. 		cualquier caracter excepto nuevas lineas

\b 		limitador (forzar a que se cumpla un patron ejemplo: \bcat\b -> si la palabra cat existe regresa true, pero si es categoria regresaria false) 

*/
// ? indica si el valor o grupo de valores es opcional 
// var expTime = /\d{1,2}-\d{1,2}-\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}/;
// () agrupacion de caracteres 

var expTimeOpcional = /\d{1,2}-\d{1,2}-\d{4}\s{1}\d{1,2}:\d{1,2}(:\d{1,2})?/;

console.log(expTimeOpcional.test('28-06-1992 20:40:50'));

console.log(expTimeOpcional.test('28-06-1992 20:40'));

console.log('21-11-1994 07:05:32'.match(expTimeOpcional));

// i es el operador no importa si es mayuscula o minuscula
// patrones repetitivos 	
var expWoohoo = /Woo+(hoo+)+/i;

console.log(expWoohoo.exec('Woohooohoohooohohooooooooo'));
console.log(expWoohoo.exec('Hola Mundo'));

var expMenuDia = /pollo|res|pescado/i;
console.log(expMenuDia)
console.log(expMenuDia.test('El cliente pidio Pollo'))

/*
^ inicio
$ fin
*/

var expIniFin = /^Any|end$/i;
console.log(expIniFin.test('Any app that can be end!'));

// g -> busca de manera global en todo el bloque de texto
var mensaje = 'El cliente pidio sushi y comio su sushi';
console.log(mensaje.replace(/sushi/g, 'tacos'));

// .search() te regresa la posicion de donde coincidio la busqueda de la expresion regular, si no eng¡cuentra ninguna incidencia regresara -1

// lastIndex

var expresion = /y/g;
